# pms-barra-menu

Componente barra de menu para aplicações Vue.

# Instalação

```bash
npm install git+https://dev.santos.sp.gov.br/publico/pms-barra-menu.git#master --save
```

# Configuração

Por lidar com rotas, o seu componente precisará ter uma breve configuração de uso do [vue-router](https://router.vuejs.org/) antes de seguir adiante. Com isso em mente, certifique-se de que o seu `main.js` possui:

**1 -** Importação do seu [arquivo de definição de rotas](https://dev.santos.sp.gov.br/publico/pms-barra-menu/blob/master/exemplos/router.js):<br>

```javascript
import router from './router'
```

**2 -** Arquivo router, importado anteriormente, informado no ponto de partida da sua aplicação Vue.js:<br>

```javascript
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
```

# Props

| Obrigatório | Nome | Tipo | Default | Formato |
|-------------|------|------|---------|---------|
| **Sim** | rotas | `Array` | | *observar modelo abaixo* |

Segue abaixo um exemplo de definição válido para o `Array` rotas, bem como seus elementos internos devem ser estruturados.

## Rotas

```javascript
rotas: [
  {
    meta: {
      menu: {
        // path: 'rota-nivel-1',
        title: 'Rota Nível 1',
        name: 'rota-nivel-1',
        children: [
          {
            title: 'Rota Nível 2',
            name: 'rota-nivel-2',
            path: 'rota-nivel-2'
          },
        ]
      }
    }
  }
]
```

**Observação:**<br>

O valor `path`, quando habilitado e preenchido corretamente, faz com que os elementos definidos em `children`, isto é, os itens do sub-menu *dropdown*, não sejam exibidos. Com isso em mente, somente insira o atributo `path` em um objeto se o mesmo não possuir rotas descendentes.

# Utilização

Ponto-chave deste componente, as rotas deverão ser definidas através do uso de [slots](https://br.vuejs.org/v2/guide/components-slots.html), sendo eles o *default* e o rota-filha. Você poderá realizar a configuração de ambos da seguinte forma:

**Slot default:**<br>

```html
<template v-slot:default="{ rota }">
  <router-link :to="rota.path">
    {{ rota.title }}
  </router-link>
</template>
```

**Slot rota-filha:**<br>

```html
<template v-slot:rota-filha="{ child }">
  <router-link :to="child.path">
    {{ child.title }}
  </router-link>
</template>
```

Os valores `rota` e `child` correspondem ao valor de cada rota sendo iterada através de um `v-for` em [src/pms-barra-menu.vue](https://dev.santos.sp.gov.br/publico/pms-barra-menu/blob/master/src/pms-barra-menu.vue#L7) que percorre o `Array` rotas que é recebido através da `prop` que foi previamente enviada.

# Demonstração do componente

Restando alguma dúvida acerca da utilização e configuração do componente, consulte o diretório [exemplos](https://dev.santos.sp.gov.br/publico/pms-barra-menu/tree/master/exemplos) para ver de perto uma amostra do componente sendo demonstrado em pleno funcionamento.