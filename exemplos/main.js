import Vue from 'vue'
import App from './App.vue'
import router from './router'

import './main.less'
// import '@pms/pms-tema/dist/pms-tema.min.css'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')