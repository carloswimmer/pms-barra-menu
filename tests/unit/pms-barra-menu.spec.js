import { shallowMount, createLocalVue } from '@vue/test-utils'
import PmsBarraMenu from '../../src/pms-barra-menu.vue'
import SlotRota from './slots/rota.vue'
import SlotRotaFilha from './slots/rota-filha.vue'
import VueRouter from 'vue-router'

let rota = {
  name: 'rota',
  title: 'Rota 1',
  children: [{
    title: 'Rota 1/1',
    path: 'caminho/rota-filha'
  }]
}

const rotas = [{
  meta: {
    menu: rota
  }
}]

const localVue = createLocalVue()
localVue.use(VueRouter)
    
let wrappers = {
  roteado: null,
  default: shallowMount(PmsBarraMenu, {
    propsData: { rotas },
    attachToDocument: true
  })
}

const dropdown = wrappers.default.find(`.barra-menu-rota-${rota.name}`)

/* eslint-disable */
describe('pms-barra-menu', () => {
  it('possui rota com nome definido', () => {
    expect(wrappers.default.find(`.${rota.name}`).exists()).toBe(true)
    expect(dropdown.exists()).toBe(true)
  })
  
  describe('possui tag <a>', () => {
    const ancora = wrappers.default.find('.dropdown-toggle')
    
    it('com atributo `data-name` definido', () =>
      expect(ancora.attributes('data-name')).toEqual(rota.name)
    )
    
    it('com texto definido', () =>
      expect(ancora.text()).toEqual(rota.title)
    )
    
    it('exibindo o conteúdo do menu dropdown quando clicado', () => {
      ancora.trigger('click')
      expect(dropdown.classes()).toContain('show')
    })
  })
  
  it('possui objeto `rotas` simplificado', () => {
    const rotasFormatadas = rotas.map(rota => rota.meta.menu)
    expect(wrappers.default.vm.rotasNoMenu).toEqual(rotasFormatadas)
  })
  
  wrappers.roteado = shallowMount(PmsBarraMenu, {
    localVue,
    slots: {
      'default': SlotRota,
      'rota-filha': SlotRotaFilha
    },
    propsData: {
      rotas: [{
        meta: {
          menu: {
            ...rota,
            path: 'caminho/rota'
          }
        }
      }]
    }
  })
  
  describe('possui slot de tipo/name', () => {
    describe('default que possui', () => {
      it('caminho definido', () => verificaSlot())
      it('título exibido', () => verificaSlot(false))
    })
    
    describe('rota-filha que possui', () => {
      const classe = 'dropdown-menu'
      
      it('caminho definido', () => verificaSlot(true, classe))
      it('título exibido', () => verificaSlot(false, classe))
    })
  })
  
  const verificaSlot = (caminho = true, classe = 'dropdown') => {
    const detalhes = (classe === 'dropdown') ?
      ({ caminho: '/caminho/rota', titulo: 'Título' }):
      ({ caminho: '/caminho/rota-filha', titulo: 'Título [filha]' })
    
    const routerLinkStub = wrappers.roteado.find(`.${classe} router-link-stub`)
    
    caminho ?
      expect(routerLinkStub.attributes('to')).toEqual(detalhes.caminho):
      expect(routerLinkStub.text()).toEqual(detalhes.titulo)
  }
})