//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var script = {
  name: 'PmsBarraMenu',
  props: {
    rotas: {
      type: Array,
      required: true
    }
  },
  computed: {
    rotasNoMenu: function rotasNoMenu() {
      return this.rotas.map(function (rota) { return rota.meta.menu; })
    }
  },
  methods: {
    toggleSubMenu: function toggleSubMenu(e) {
      var name = e.target.dataset.name;
      var menu = document.querySelector((".barra-menu-rota-" + name));
      
      menu.classList.toggle('show');
    }
  }
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier
/* server only */
, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
  if (typeof shadowMode !== 'boolean') {
    createInjectorSSR = createInjector;
    createInjector = shadowMode;
    shadowMode = false;
  } // Vue.extend constructor export interop.


  var options = typeof script === 'function' ? script.options : script; // render functions

  if (template && template.render) {
    options.render = template.render;
    options.staticRenderFns = template.staticRenderFns;
    options._compiled = true; // functional template

    if (isFunctionalTemplate) {
      options.functional = true;
    }
  } // scopedId


  if (scopeId) {
    options._scopeId = scopeId;
  }

  var hook;

  if (moduleIdentifier) {
    // server build
    hook = function hook(context) {
      // 2.3 injection
      context = context || // cached call
      this.$vnode && this.$vnode.ssrContext || // stateful
      this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
      // 2.2 with runInNewContext: true

      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__;
      } // inject component styles


      if (style) {
        style.call(this, createInjectorSSR(context));
      } // register component module identifier for async chunk inference


      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier);
      }
    }; // used by ssr in case component is cached and beforeCreate
    // never gets called


    options._ssrRegister = hook;
  } else if (style) {
    hook = shadowMode ? function () {
      style.call(this, createInjectorShadow(this.$root.$options.shadowRoot));
    } : function (context) {
      style.call(this, createInjector(context));
    };
  }

  if (hook) {
    if (options.functional) {
      // register for functional component in vue file
      var originalRender = options.render;

      options.render = function renderWithStyleInjection(h, context) {
        hook.call(context);
        return originalRender(h, context);
      };
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate;
      options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
    }
  }

  return script;
}

var normalizeComponent_1 = normalizeComponent;

var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
  return function (id, style) {
    return addStyle(id, style);
  };
}
var HEAD;
var styles = {};

function addStyle(id, css) {
  var group = isOldIE ? css.media || 'default' : id;
  var style = styles[group] || (styles[group] = {
    ids: new Set(),
    styles: []
  });

  if (!style.ids.has(id)) {
    style.ids.add(id);
    var code = css.source;

    if (css.map) {
      // https://developer.chrome.com/devtools/docs/javascript-debugging
      // this makes source maps inside style tags work properly in Chrome
      code += '\n/*# sourceURL=' + css.map.sources[0] + ' */'; // http://stackoverflow.com/a/26603875

      code += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) + ' */';
    }

    if (!style.element) {
      style.element = document.createElement('style');
      style.element.type = 'text/css';
      if (css.media) { style.element.setAttribute('media', css.media); }

      if (HEAD === undefined) {
        HEAD = document.head || document.getElementsByTagName('head')[0];
      }

      HEAD.appendChild(style.element);
    }

    if ('styleSheet' in style.element) {
      style.styles.push(code);
      style.element.styleSheet.cssText = style.styles.filter(Boolean).join('\n');
    } else {
      var index = style.ids.size - 1;
      var textNode = document.createTextNode(code);
      var nodes = style.element.childNodes;
      if (nodes[index]) { style.element.removeChild(nodes[index]); }
      if (nodes.length) { style.element.insertBefore(textNode, nodes[index]); }else { style.element.appendChild(textNode); }
    }
  }
}

var browser = createInjector;

/* script */
var __vue_script__ = script;

/* template */
var __vue_render__ = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c("section", { attrs: { role: "menu", "data-pms": "barra-menu" } }, [
    _c("nav", { staticClass: "navbar navbar-default" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "container-fluid" }, [
          _c(
            "ul",
            { staticClass: "nav navbar-nav" },
            _vm._l(_vm.rotasNoMenu, function(rota) {
              return _c(
                "li",
                { key: "barra-menu-" + rota.name, staticClass: "dropdown" },
                [
                  rota.path
                    ? _vm._t("default", null, { rota: rota })
                    : _c(
                        "a",
                        {
                          class: "dropdown-toggle " + rota.name + " Ativa",
                          attrs: {
                            href: "#",
                            role: "button",
                            "aria-haspopup": "true",
                            "aria-expanded": "false",
                            "data-toggle": "dropdown",
                            "data-name": rota.name
                          },
                          on: {
                            click: function($event) {
                              return _vm.toggleSubMenu($event)
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n              " +
                              _vm._s(rota.title) +
                              "\n            "
                          )
                        ]
                      ),
                  _vm._v(" "),
                  _c(
                    "ul",
                    {
                      class:
                        "dropdown-menu sub-barra-menu barra-menu-rota-" +
                        rota.name
                    },
                    _vm._l(rota.children, function(child) {
                      return _c(
                        "li",
                        { key: "barra-menu-sub-" + child.name },
                        [
                          child.path
                            ? _vm._t("rota-filha", null, { child: child })
                            : _vm._e()
                        ],
                        2
                      )
                    }),
                    0
                  )
                ],
                2
              )
            }),
            0
          )
        ])
      ])
    ])
  ])
};
var __vue_staticRenderFns__ = [];
__vue_render__._withStripped = true;

  /* style */
  var __vue_inject_styles__ = function (inject) {
    if (!inject) { return }
    inject("data-v-4accf1c6_0", { source: "[data-pms=barra-menu] .navbar {\n  margin-bottom: 0px;\n  min-height: 53px;\n  position: relative;\n}\n[data-pms=barra-menu] .navbar-default {\n  background: #00615c none repeat scroll 0 0;\n  border-color: #00615c;\n  border-bottom: 1px solid #004f4a;\n  color: #ffffff;\n  border-radius: 0;\n  margin-bottom: 20px;\n  width: 100%;\n  height: 53px;\n  position: fixed;\n  left: 0;\n  z-index: 900;\n}\n[data-pms=barra-menu] ul {\n  font-size: 1.8rem;\n  font-weight: 400;\n  line-height: 2.4rem;\n  margin: 0 0 2.2rem;\n}\n[data-pms=barra-menu] .nav > li {\n  display: block;\n  margin-top: 8px;\n  position: relative;\n}\n[data-pms=barra-menu] .navbar-nav > li > a {\n  border-radius: 0;\n  color: #a1be4d !important;\n  display: block;\n  font-size: 0.85em;\n  font-weight: bold;\n  padding: 10px 15px;\n  position: relative;\n  text-transform: uppercase;\n}\n[data-pms=barra-menu] .navbar-nav > li > a.active,\n[data-pms=barra-menu] .navbar-nav > li > a:focus,\n[data-pms=barra-menu] .navbar-nav > li > a:hover,\n[data-pms=barra-menu] .navbar-nav > li > a:visited {\n  background-color: rgba(0, 0, 0, 0);\n  border-bottom: 3px solid;\n  text-decoration: none;\n  border-radius: 0;\n  color: #a1be4d !important;\n  display: block;\n  font-size: 0.85em;\n  font-weight: bold;\n  padding: 10px 15px;\n  position: relative;\n  text-transform: uppercase;\n}\n[data-pms=barra-menu] .dropdown-menu {\n  font-size: 0.9em;\n}\n[data-pms=barra-menu] .navbar-default .navbar-nav > .open > a,\n[data-pms=barra-menu] .navbar-default .navbar-nav > .open > a:hover,\n[data-pms=barra-menu] .navbar-default .navbar-nav > .open > a:focus,\n[data-pms=barra-menu] .navbar-default .navbar-nav > .open > a:active {\n  background-color: rgba(0, 0, 0, 0);\n  color: #a1be4d !important;\n}\n[data-pms=barra-menu] .dropdown-menu > li > a {\n  color: #468B35;\n}\n[data-pms=barra-menu] .dropdown-menu > li > a:hover,\n[data-pms=barra-menu] .dropdown-menu > li > a:focus {\n  text-decoration: none;\n  color: #A1BE4D;\n  background-color: #eefceb;\n}\n@media screen and (min-width: 768px) {\n[data-pms=barra-menu] .navbar-default {\n    background: #00615c none repeat scroll 0 0;\n    border-color: #00615c;\n    border-bottom: 1px solid #004f4a;\n    color: #ffffff;\n    border-radius: 0;\n    margin-bottom: 20px;\n    width: 100%;\n    height: 53px;\n    position: fixed;\n    left: 0;\n}\n}\n", map: undefined, media: undefined });

  };
  /* scoped */
  var __vue_scope_id__ = undefined;
  /* module identifier */
  var __vue_module_identifier__ = undefined;
  /* functional template */
  var __vue_is_functional_template__ = false;
  /* style inject SSR */
  

  
  var pmsBarraMenu = normalizeComponent_1(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    browser,
    undefined
  );

export default pmsBarraMenu;
